<?php

namespace Anchu\Restful\Commands;

use Illuminate\Console\Command;

/**
 * @register
 *      register in app/Console/Kernel.php
 *      $this->load(base_path('vendor/hzanchu/restful') . '/Commands');
 *
 * @uses
 *      php artisan table table_name create    // 使用表名
 *      php artisan table TableModel create    // 使用模型类，默认到app/Models里去找
 *      php artisan table Anchu\Restful\Models\Demos\TestModel create    // 使用命名空间
 * Class Table
 * @package App\Restful\Commands
 */
class Table extends Command
{
    /**
     * @example
     *      php artisan table TestModel create    // 创建表
     *      php artisan table TestModel drop      // 删除表
     *      php artisan table TestModel recreate  // 重建表（先删除再创建）
     *      php artisan table TestModel show rule // 显示表单校验规则
     *      php artisan table TestModel show attr // 显示字段名称，用户Request.attributes()中的表单字段名称
     *      php artisan table TestModel show defaults // 显示字段在表中的默认值
     * @var string operate
     */
    protected $signature = 'table {table} {operate}';

    protected $description = 'Show the table details and operate it.';

    protected $defaultModelsPath = 'App\Models';

    private function getModelClass()
    {
        $table = $this->argument('table');
        if (strpos(strtolower($table), 'model') === false) {
            // 实现通过表名查找模型类：user -> App\Models\UserModel
            $class = $this->defaultModelsPath . '\\' . $this->camelize($table) . 'Model';
        } elseif (strpos($table, '\\') === false) {
            // 实现通过模型类名称查找模型类： UserModel -> App\Models\UserModel
            $class = $this->defaultModelsPath . '\\' . $table;
        } else {
            // 通过命名空间查找模型类
            $class = $table;
        }

        if (!class_exists($class)) {
            $this->error('这个类不存在：' . $class);
            exit(0);
        }

        // 判断类的继承关系
        if (strpos(get_parent_class($class), 'Restful\Models\Model') === false) {
            $this->error('这个模型类必须要继承于Anchu\Restful\Models\Model类，而它实际上继承于：' . get_parent_class($class));
            exit(0);
        }
        return $class;
    }

    /**
     * 下划线转换为驼峰
     * @param $data
     * @return mixed
     */
    public function camelize($camelCaps, $separator = '_')
    {
        $items = explode($separator, $camelCaps);
        $result = '';
        foreach ($items as $key => $item) {
            $result .= ucfirst(strtolower($item));
        }
        return $result;
    }

    public function handle()
    {
        $table = $this->getModelClass();
        $operate = $this->argument('operate');

        switch ($operate) {
            case 'recreate':
            case 'create':
                $table::createTable();
                $this->info('This Table Created Successfully!');
                break;
            case 'drop':
                $table::dropTable();
                $this->info('This Table Dropped Successfully!');
                break;
            case 'rules':
                var_dump($table::rules());
                break;
            case 'attributes':
                var_dump($table::attributes());
                break;
            case 'defaults':
                var_dump($table::defaults());
                break;
        }
    }
}
