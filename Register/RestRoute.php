<?php


namespace Anchu\Restful\Register;

use Illuminate\Routing\ResourceRegistrar;

class RestRoute extends ResourceRegistrar
{
    protected $resourceDefaults = ['index', 'view', 'create', 'update', 'delete'];

}
