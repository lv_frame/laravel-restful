<?php


namespace Anchu\Restful\Runner;

use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @access 需要登录才能访问
 * Class IndexRunner
 * @package Anchu\Restful\Runner
 */
class IndexRunner extends Runner
{
    public function run(Request $request)
    {
        $this->beforeRun();
        $this->result = $this->repository->getList(
            where: $this->filter->where,
            field: $this->filter->field,
            orderBy: $this->filter->orderBy,
            perPage: $this->filter->perPage,
            with: $this->filter->with,
            page: $this->filter->currentPage
        );
        $this->afterRun();
        return $this->result;
    }
}
