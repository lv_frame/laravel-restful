<?php

namespace Anchu\Restful\Runner;

use Illuminate\Http\Request;

class ViewRunner extends Runner
{
    public function run(Request $request)
    {
        $this->beforeRun();
        $id = $request->route('id', 0);
        $this->result = $this->repository->getById($id, $this->filter->with);
        $this->afterRun();
        return $this->result;
    }
}
