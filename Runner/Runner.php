<?php

namespace Anchu\Restful\Runner;

use Anchu\Restful\Repository\Repository;
use Anchu\Restful\Runner\Decorates\DecorateFactory;
use Anchu\Restful\Runner\Decorates\Filter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

abstract class Runner
{
    public Filter $filter;
    public $model = '';
    protected Repository $repository;
    protected array $decorates;
    protected $result;
    protected array $params;

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function setDecorates($decorates)
    {
        $this->decorates = $decorates;
    }

    public function setRepository(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * 尽量在这里完成数据的整理
     * 举例：
     *      1. user_id/admin_id的设置；
     *      2. 创建点赞记录的时候要先删除原来的点赞记录，防止重复点赞；
     */
    public function beforeRun()
    {
        // TODO: do something if necessary
        $this->filter = DecorateFactory::filter($this->decorates['filters']);
    }

    /**
     * 尽量在这里完成结果集的整理
     */
    public function afterRun()
    {
        // TODO: do something if necessary
        $this->result = DecorateFactory::data($this->decorates['after'], $this->result);
    }

    abstract public function run(FormRequest|Request $request);

}
