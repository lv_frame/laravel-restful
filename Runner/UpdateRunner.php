<?php


namespace Anchu\Restful\Runner;

use Anchu\Restful\Runner\Decorates\DecorateFactory;
use Illuminate\Http\Request;

class UpdateRunner extends Runner
{

    public function beforeRun()
    {
        parent::beforeRun(); // TODO: Change the autogenerated stub
        $this->params = request()->post() ?? [];
        $this->params = DecorateFactory::params($this->decorates['params'], $this->params);
    }

    /**
     * 尽量在这里完成数据的整理
     */
    public function run(Request $request)
    {
        $this->beforeRun();
        $this->repository->update(
            $request->route("id"),
            $this->params,
            $this->filter->where
        );
        $this->afterRun();
        return $this->result;
    }
}
