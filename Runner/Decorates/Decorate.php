<?php

namespace Anchu\Restful\Runner\Decorates;

interface Decorate
{
    public function run(mixed $input, array $options);
}

