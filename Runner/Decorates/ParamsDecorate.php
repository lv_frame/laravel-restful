<?php


namespace Anchu\Restful\Runner\Decorates;

abstract class ParamsDecorate
{
    abstract public function run($params, $options);
}