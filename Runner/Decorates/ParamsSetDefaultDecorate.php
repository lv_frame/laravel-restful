<?php


namespace Anchu\Restful\Runner\Decorates;

use Anchu\Restful\Models\Model;

/**
 * 在创建记录的时候，如果前端传入的数据是null，而数据库表的设计一般设置为not null，
 * 此时会导致报错，因此在这个装饰器中进行默认值转换。
 *
 * Class ParamsSetDefaultDecorate
 * @package Anchu\Restful\Runner\Decorates
 */
class ParamsSetDefaultDecorate extends ParamsDecorate
{
    public function run($params, $options): array
    {
        return isset($options['model']) ? $this->setDefault($params, $options['model']) : $params;
    }

    protected function setDefault($params, $model)
    {
        if (!class_exists($model)) {
            return $params;
        }
        $defaultValue = $model::defaults();
        foreach ($params as $key => $value) {
            if (!isset($defaultValue[$key])) { // 要插入的数据字段不存在，就取消
                unset($params[$key]);
            } else {
                $params[$key] = is_null($value) ? $defaultValue[$key] : $params[$key];
            }
        }
        return $params;
    }
}