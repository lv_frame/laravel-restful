<?php

namespace Anchu\Restful\Runner\Decorates;

class FilterDecorate
{
    /**
     * 要求返回Filter对象
     * @param $filter Filter
     * @return Filter
     */
    public function run(Filter $filter, array $options = []): Filter
    {
        return $filter;
    }
}
