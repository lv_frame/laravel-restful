<?php


namespace Anchu\Restful\Runner\Decorates;

class DataUnSensibleDecorate
{
    public function run(array $data): array
    {
        return $this->unSensible($data);
    }

    /**
     * 脱敏
     */
    public function unSensible($data = [])
    {
        foreach ($data as $key => $item) {
            if (is_array($item)) {
                $data[$key] = $this->unSensible($item);
            } else {
                if (($key == 'mobile' || $key == 'identify') && strlen($item) > 7) {
                    $data[$key] = substr($item, 0, 3) . str_repeat('*', strlen($item) - 7) . substr($item, -4);
                }
            }
        }
        return $data;
    }
}
