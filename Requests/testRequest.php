<?php


namespace Anchu\Restful\Requests;


use App\Models\TestModel;
use Anchu\Restful\Models\Model;

class testRequest extends Request
{
    public string|Model $model = TestModel::class;
}
