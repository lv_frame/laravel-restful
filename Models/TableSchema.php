<?php


namespace Anchu\Restful\Models;


use Anchu\Restful\Models\Columns\Column;
use Anchu\Restful\Models\Keys\IKey;


class TableSchema
{
    /**
     * @param string $tableName
     * @param string $comment
     * @param $columns Column[]
     * @param string $engine
     * @param string $charset
     * @param $keys IKey[]
     * @param bool $softDelete
     */
    public function __construct(
        public string $tableName,   // 表名
        public string $comment,     // 表说明
        public array $columns,
        public string $engine = 'innodb',
        public string $charset = 'utf8mb4',
        public array $keys = [],
        public bool $softDelete = true
    )
    {

    }
}
