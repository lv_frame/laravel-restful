<?php

namespace Anchu\Restful\Models\Columns;

/**
 * 定义字符串类型的字段
 * @package Anchu\Restful\Models\Columns
 */
class CBigInteger extends CInteger
{
    public function __construct(
        string $label,
        string $comment = '',
        bool $null = false,
        int $default = 0,
        bool $unsigned = true,
        string $rule = ''
    )
    {
        parent::__construct(
            label: $label,
            comment: $comment,
            type: 'bigInteger',
            null: $null,
            default: $default,
            unsigned: $unsigned,
            rule: $rule
        );
    }

    // 无符号取值范围：0 - 18446744073709551615
    // 有符号取值范围：-9223372036854775808 - 9223372036854775807
    public function rule()
    {
        return $this->rule != '' ? $this->rule : ($this->unsigned ? 'integer|min:0|max:' . 1 << 64 : 'integer|min:-'. 1 << 63 .'|max:' . (1 << 63 - 1));
    }
}
