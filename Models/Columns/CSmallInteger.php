<?php

namespace Anchu\Restful\Models\Columns;

/**
 * 定义字符串类型的字段
 * @package Anchu\Restful\Models\Columns
 */
class CSmallInteger extends CInteger
{
    public function __construct(
        string $label,
        string $comment = '',
        bool $null = false,
        int $default = 0,
        bool $unsigned = true,
        string $rule = ''
    )
    {
        parent::__construct(
            label: $label,
            comment: $comment,
            type: 'smallInteger',
            null: $null,
            default: $default,
            unsigned: $unsigned,
            rule: $rule
        );
    }

    // 无符号取值范围：0 - 32768
    // 有符号取值范围：-32768 - 32767
    public function rule()
    {
        return $this->rule != '' ? $this->rule : ($this->unsigned ? 'integer|min:0|max:65535' : 'integer|min:-32768|max:32767');
    }
}
