<?php

namespace Anchu\Restful\Models\Columns;

/**
 * 定义字符串类型的字段
 * @package Anchu\Restful\Models\Columns
 */
class CMediumInteger extends CInteger
{
    public function __construct(
        string $label,
        string $comment = '',
        bool $null = false,
        int $default = 0,
        bool $unsigned = true,
        string $rule = ''
    )
    {
        parent::__construct(
            label: $label,
            comment: $comment,
            type: 'mediumInteger',
            null: $null,
            default: $default,
            unsigned: $unsigned,
            rule: $rule
        );
    }

    // 无符号取值范围：0 - 16777215
    // 有符号取值范围：-8388608 - 8388607
    public function rule()
    {
        return $this->rule != '' ? $this->rule : ($this->unsigned ? 'integer|min:0|max:16777215' : 'integer|min:-8388608|max:8388607');
    }
}
