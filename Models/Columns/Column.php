<?php

namespace Anchu\Restful\Models\Columns;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * @function 定义字段的格式和规则
 * @package Anchu\Restful\Models\Columns
 * @method static \Illuminate\Database\Schema\Builder create(string $table, \Closure $callback)
 */
abstract class Column
{

    abstract public function createColumn(string $tableName, string $columnName);

    /**
     * @function 根据字段的定义，创建校验规则
     * @return array
     */
    public function rule()
    {
        // TODO: Implement rules() method.
        return $this->rule;
    }

    /**
     * 字段的属性
     * @return array
     */
    public function attribute()
    {
        // TODO: Implement attributes() method.
        return $this->label;
    }
}
