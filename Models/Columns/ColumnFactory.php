<?php


namespace Anchu\Restful\Models\Columns;

/**
 * @method CString string(string $label, string $type = 'string', int $length = 1000, bool $null = false, string $default = '', string $comment = '', string $rule = '')
 * @method CVarchar varchar(string $label, int $length = 1000, bool $null = false, string $default = '', string $comment = '', string $rule = '')
 * @method CChar char(string $label, int $length = 255, bool $null = false, string $default = '', string $comment = '', string $rule = '')
 * @method CInteger integer(string $label, string $type = 'integer', bool $unsigned = true, bool $null = false, string $default = 0, string $comment = '', string $rule = 'integer')
 * @method CTinyInteger tinyInteger(string $label, bool $unsigned = true, bool $null = false, string $default = 0, string $comment = '', string $rule = '')
 * @method CSmallInteger smallInteger(string $label, bool $unsigned = true, bool $null = false, string $default = 0, string $comment = '', string $rule = '')
 * @method CMediumInteger mediumInteger(string $label, bool $unsigned = true, bool $null = false, string $default = 0, string $comment = '', string $rule = '')
 * @method CBigInteger bigInteger(string $label, bool $unsigned = true, bool $null = false, string $default = 0, string $comment = '', string $rule = '')
 * @method CNumeric numeric(string $label, string $comment = '', string $type = 'decimal', int $precision = 8, int $scale = 2, bool $null = false, string $default = 0, bool $unsigned = false, string $rule = '')
 * @method CFloat float(string $label, string $comment = '', int $precision = 8, int $scale = 2, bool $null = false, string $default = 0, bool $unsigned = false, string $rule = '')
 * @method CDouble double(string $label, string $comment = '', int $precision = 8, int $scale = 2, bool $null = false, string $default = 0, bool $unsigned = false, string $rule = '')
 * @method CDecimal decimal(string $label, string $comment = '', int $precision = 8, int $scale = 2, bool $null = false, string $default = 0, bool $unsigned = false, string $rule = '')
 * @method CText text(string $label, string $comment = '', string $type = 'text', bool $null = false, string $rule = 'string')
 * @method CEnum enum(string $label, array $in, string $comment = '', bool $null = false, string $default = '')
 * @method CTimestamp timestamp(string $label, string $comment = '', bool $null = false, string $default = '')
 * Class ColumnFactory
 * @package Anchu\Restful\Models\Columns
 */
class ColumnFactory
{
    /**
     * @function 使用laravel提供的类创建器创建字段
     * @param string $name
     * @param array $arguments
     * @return Column
     */
    public function __call(string $name, array $arguments)
    {
        // TODO: Implement __call() method.
        $class = __NAMESPACE__ . '\C' . ucfirst($name);
        return app($class, $arguments);
    }

    // 配置一些常用的字段

    /**
     * @function 性别，1：男，2：女
     * @return CTinyInteger
     * @example $this->gender();
     */
    public function gender()
    {
        return new CTinyInteger(label: '性别', comment: '1:男，2:女', default: '1');
    }

    /**
     * @function 联系人
     * @return CVarchar
     * @example $this->person();
     */
    public function person()
    {
        return new CVarchar(label: '联系人', length: 50);
    }

    /**
     * @function 联系方式
     * @return CVarchar
     * @example $this->mobile();
     */
    public function mobile()
    {
        return new CVarchar(label: '联系方式', length: 50);
    }

    /**
     * @function 状态
     * @param string[] $items
     * @return CTinyInteger
     * @example $this->status();
     */
    public function status($items = [1 => '提交', 2 => '审核', 3 => '驳回'])
    {
        $comment = '';
        $in = '';
        foreach ($items as $key => $value) {
            $comment .= $key . ':' . $value . ',';
            $in .= $key . ',';
        }
        $comment = substr($comment, 0, -1);
        $in = substr($in, 0, -1);
        $default = array_key_first($items);
        return new CTinyInteger(label: '状态', comment: $comment, default: $default, rule: 'integer|in:' . $in);
    }
}
