<?php

namespace Anchu\Restful\Models\Columns;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * 定义字符串类型的字段
 * @package Anchu\Restful\Models\Columns
 */
class CText extends Column
{
    public $default;

    /**
     * CString constructor.
     * @param string $type : tinyText/text/mediumText/longText
     * @param bool $null : 是否允许空值
     * @param string $label : 字段的名称，用于校验时的提示
     * @param string $comment : 字段的附属说明，如：status:状态，1:保存；2:发布（上架）；3：已下架；4:违规下架
     * @param string $rule : 字段的校验规则：'string|max:1000'
     */
    public function __construct(
        public string $label, // 属性名称：必填
        public string $type = 'text',
        public bool $null = false,
        public string $comment = '',
        public string $rule = 'string'
    )
    {
        // 这样做的目的是为了将label和comment分开：
        // $label : 状态
        // $comment : 1：提交， 2：通过，3：驳回
        // $this->comment = 状态 1：提交， 2：通过，3：驳回
        $this->comment = $comment == '' ? $label : trim($label . ' ' . $comment);
        $this->default = null;
    }

    /**
     * @inheritDoc
     */
    public function rule()
    {
        // TODO: Implement rules() method.
        return $this->rule;
    }

    public function createColumn($tableName, $columnName)
    {
        $context = $this;
        Schema::table($tableName, function (Blueprint $table) use ($context, $columnName) {
            $type = $context->type;
            $table->$type($columnName)
                ->nullable($context->null)
                ->comment($context->comment);
        });
    }

}
