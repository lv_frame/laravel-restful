<?php

namespace Anchu\Restful\Models\Columns;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * 定义字符串类型的字段
 * @package Anchu\Restful\Models\Columns
 */
class CNumeric extends Column
{
    /**
     * CString constructor.
     * @param string $type : float/double/decimal
     * @param int $precision : 有效字数总位数
     * @param int $scale : 小数部分总位数
     * @param bool $null : 是否允许空值
     * @param float $default : 默认值设置
     * @param string $label : 字段的名称，用于校验时的提示
     * @param string $comment : 字段的附属说明，如：status:状态，1:保存；2:发布（上架）；3：已下架；4:违规下架
     * @param bool $unsigned : 默认设置为false，支持负数
     * @param string $rule : 字段的校验规则：'numeric'
     */
    public function __construct(
        public string $label,
        public string $comment = '',
        public string $type = 'decimal',
        public int $precision = 8,
        public int $scale = 2,
        public bool $null = false,
        public int $default = 0,
        public bool $unsigned = false,
        public string $rule = 'numeric'
    )
    {
        // 这样做的目的是为了将label和comment分开：
        // $label : 状态
        // $comment : 1：提交， 2：通过，3：驳回
        // $this->comment = 状态 1：提交， 2：通过，3：驳回
        $this->comment = $comment == '' ? $label : trim($label . ' ' . $comment);
    }

    /**
     * @inheritDoc
     */
    public function rule()
    {
        // TODO: Implement rules() method.
        return $this->rule;
    }

    /**
     * 用于migrate建表操作，integer类型的字段不用设置长度
     * @param string $tableName
     * @param string $columnName
     */
    public function createColumn($tableName, $columnName)
    {
        $context = $this;
        Schema::table($tableName, function (Blueprint $table) use ($context, $columnName) {
            $type = $context->type;
            // 没有设置length的功能
            $table->$type($columnName, $context->precision, $context->scale, $context->unsigned)
                ->nullable($context->null)
                ->default($context->default)
                ->comment($context->comment);
        });
    }
}
