<?php

namespace Anchu\Restful\Models\Columns;

use Illuminate\Database\Schema\Blueprint;

/**
 * 可变长字段
 */
class CVarchar extends CString
{
    public function __construct(
        string $label, // 字段名称：必填
        int $length = 1000,
        bool $null = false,
        string $default = '',
        string $comment = '',
        string $rule = ''
    )
    {
        parent::__construct($label, $type = 'string', $length, $null, $default, $comment, $rule);
    }
}
