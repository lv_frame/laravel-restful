<?php

namespace Anchu\Restful\Models\Keys;

abstract class IKey
{
    /**
     * 创建索引
     * @return mixed
     */
    abstract public function createKey(string $tableName, string $keyName);
}
