<?php

namespace Anchu\Restful\Models\Keys;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Key extends IKey
{
    public function __construct(
        public array|string $column, // 字段集
        public bool $unique = false
    )
    {
    }

    /**
     * 创建索引
     * @param $tableName
     * @return mixed
     */
    public function createKey($tableName, $keyName) {
        $context = $this;
        Schema::table($tableName, function (Blueprint $table) use ($context, $keyName) {
            if (!$context->unique) {
                // 创建普通索引
                $table->index($context->column,  $keyName);
            } else {
                // 创建唯一值索引
                $table->unique($context->column,  $keyName);
            }
        });
    }
}
